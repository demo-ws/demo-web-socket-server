<?php

declare(strict_types=1);

namespace App\Utils;

use Ramsey\Uuid\UuidFactoryInterface;
use SocketIO\Utils\Uuid4GeneratorInterface;

class UuidFactoryAdapter implements Uuid4GeneratorInterface
{
    private UuidFactoryInterface $factory;

    public function __construct(UuidFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function getUuid(): string
    {
        return $this->factory->uuid4()->toString();
    }
}
