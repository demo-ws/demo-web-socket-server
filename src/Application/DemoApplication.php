<?php

declare(strict_types=1);

namespace App\Application;

use SocketIO\Application\ApplicationInterface;
use SocketIO\Server\Connection\Request;

class DemoApplication implements ApplicationInterface
{

    public function canHandleRequest(Request $request): bool
    {
        return true;
    }

    public function isAuthorizedRequest(Request $request): bool
    {
        return true;
    }

    public function isDefault(): bool
    {
        return true;
    }
}
