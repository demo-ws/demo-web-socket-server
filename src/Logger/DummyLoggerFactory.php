<?php

declare(strict_types=1);

namespace App\Logger;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class DummyLoggerFactory implements SocketIOLoggerFactoryInterface
{

    public function createServerLogger(): LoggerInterface
    {
        return new NullLogger();
    }

    public function createConnectionLogger(): LoggerInterface
    {
        return new NullLogger();
    }

    public function createStateLogger(): LoggerInterface
    {
        return new NullLogger();
    }
}
