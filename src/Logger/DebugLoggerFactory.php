<?php

declare(strict_types=1);

namespace App\Logger;

use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\AbstractHandler;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class DebugLoggerFactory implements SocketIOLoggerFactoryInterface
{
    public function createServerLogger(): LoggerInterface
    {
        return new Logger('socketio.server', $this->getHandlers());
    }

    public function createConnectionLogger(): LoggerInterface
    {
        return new Logger('socketio.connection', $this->getHandlers());
    }

    public function createStateLogger(): LoggerInterface
    {
        return new Logger('socketio.state', $this->getHandlers());
    }

    /**
     * @return AbstractHandler[]
     */
    protected function getHandlers(): array
    {
        $handler = new StreamHandler($this->getStdErrHandle(), Logger::DEBUG);
        $handler->setFormatter($this->getFormatter());
        return [$handler];
    }

    /**
     * @return resource
     */
    protected function getStdErrHandle()
    {
        static $fh = null;
        $fh ??= $fh = fopen('php://stderr', 'w+');
        return $fh;
    }

    protected function getFormatter(): FormatterInterface
    {
        $formatter = new LineFormatter();
        $formatter->includeStacktraces();
        $formatter->ignoreEmptyContextAndExtra();
        return $formatter;
    }
}
