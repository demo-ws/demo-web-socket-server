<?php

declare(strict_types=1);

namespace App\Logger;

use Psr\Log\LoggerInterface;

interface SocketIOLoggerFactoryInterface
{
    public function createServerLogger(): LoggerInterface;

    public function createConnectionLogger(): LoggerInterface;

    public function createStateLogger(): LoggerInterface;
}
