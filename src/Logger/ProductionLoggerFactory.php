<?php

declare(strict_types=1);

namespace App\Logger;

use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\AbstractHandler;
use Monolog\Handler\Handler;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class ProductionLoggerFactory extends DebugLoggerFactory
{
    /**
     * @return AbstractHandler[]
     */
    protected function getHandlers(): array
    {
        return \array_map(
            static fn (AbstractHandler $handler) => $handler->setLevel(Logger::WARNING),
            parent::getHandlers()
        );
    }
}
