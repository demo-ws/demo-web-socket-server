<?php

declare(strict_types=1);

require_once 'vendor/autoload.php';

if (($_ENV['APP_DEBUG'] ?? 'false') === 'true') {
    $loggerFactory = new \App\Logger\DebugLoggerFactory();
} else {
    $loggerFactory = new \App\Logger\ProductionLoggerFactory();
}

$serverConfiguration = new \SocketIO\Server\ServerConfiguration(
    (int)($_ENV['APP_PORT'] ?? 8000),
    '0.0.0.0'
);

$uuid4Generator = new \App\Utils\UuidFactoryAdapter(new \Ramsey\Uuid\UuidFactory());

$stateFactory = new \SocketIO\Server\Connection\State\StateFactory($uuid4Generator);
$stateFactory->setLogger($loggerFactory->createStateLogger());

$connectionFactory = new \SocketIO\Server\Connection\ConnectionFactory($stateFactory);
$connectionFactory->setLogger($loggerFactory->createConnectionLogger());

$server = new \SocketIO\Server\Server(
    $serverConfiguration,
    $connectionFactory,
);
$server->setLogger($loggerFactory->createServerLogger());

$baseApp = new \App\Application\DemoApplication();
$server->registerApplication($baseApp);

pcntl_async_signals(true);
pcntl_signal(SIGINT, static fn () => $server->stop());
pcntl_signal(SIGTERM, static fn () => $server->stop());

$server->run();
