FROM registry.gitlab.com/demo-ws/containers/php-prod-7.4:latest AS php-websocket-server

ENV APP_PORT=8000

RUN mkdir -p /var/www/websocket-server &&\
        adduser -g socket-server --disabled-password --no-create-home socket-server

WORKDIR /var/www/websocket-server

COPY . .

USER socket-server:socket-server

CMD php index.php

EXPOSE 8000