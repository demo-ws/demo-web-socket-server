<?php

declare(strict_types=1);

$targetDir = __DIR__ . '/' . 'build';
$appName = 'socket.io-app';
$targetPhar = $targetDir . '/' . $appName . '.phar';

$options = getopt('', ['compress']);
$useCompression = isset($options['compress']);


$isReadOnly = ini_get('phar.readonly');
if ($isReadOnly) {
    throw new \LogicException(
        'Cannot create phar, cause phar is in readonly mode! Check \'phar.readonly\' php.ini option.'
    );
}

if (!file_exists($targetDir)) {
    if (!is_writable(__DIR__)) {
        throw new \LogicException('Cannot create build directory! Project directory is not writable!');
    }
    mkdir($targetDir);
} elseif (!is_writable($targetDir)) {
    throw new \LogicException('Build directory is not writable!');
}
Phar::unlinkArchive($targetPhar);
$pharBuilder = new Phar(
    $targetPhar,
    FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS,
    $appName
);

$requiredPackages = getRequiredPackages('composer.json', 'composer.lock');

$iterator = new AppendIterator();
$iterator->append(new RecursiveIteratorIterator(new RecursiveDirectoryIterator('src')));
foreach ($requiredPackages as $package) {
    $iterator->append(new RecursiveIteratorIterator(new RecursiveDirectoryIterator('vendor/' . $package)));
}
$iterator->append(new GlobIterator('./*.*'));
$iterator = new CallbackFilterIterator($iterator, static fn (SplFileInfo $fileInfo): bool => !$fileInfo->isDir());

$pharBuilder->startBuffering();


$pharBuilder->buildFromIterator($iterator, __DIR__);

if ($useCompression) {
    $availableCompressions = Phar::getSupportedCompression();
    if (in_array(Phar::BZ2, $availableCompressions)) {
        $pharBuilder->compress(Phar::BZ2);
    } elseif (in_array(Phar::GZ, $availableCompressions)) {
        $pharBuilder->compress(Phar::BZ2);
    }
}

$pharBuilder->setStub(Phar::createDefaultStub('index.php'));

$pharBuilder->stopBuffering();


/**
 * @return array<string, string>
 * @throws JsonException
 */
function getRequiredPackages(string $composerJsonPath, string $composerLockPath): array
{
    $excludePhpExtensionsFilter = static fn (string $packageName): bool =>
        !preg_match('/^(php|ext-[^\/]+)$/', $packageName);

    if (!is_readable($composerJsonPath) || !is_readable($composerLockPath)) {
        throw new \LogicException('composer.json or composer.lock is not readable!');
    }

    $composerJsonData = json_decode(
        file_get_contents($composerJsonPath) ?: '',
        true,
        512,
        JSON_THROW_ON_ERROR
    );
    $composerLockData = json_decode(
        file_get_contents($composerLockPath) ?: '',
        true,
        512,
        JSON_THROW_ON_ERROR
    );

    /** @var array<string> $requirePackages */
    $requirePackages = array_filter(
        array_keys($composerJsonData['require'] ?? []),
        $excludePhpExtensionsFilter
    );

    /** @var array<string, array<string>> $packageDependencies */
    $packageDependencies = [];
    foreach ($composerLockData['packages'] ?? [] as $packageDetail) {
        $packageDependencies[$packageDetail['name']] = array_filter(
            array_keys($packageDetail['require'] ?? []),
            $excludePhpExtensionsFilter
        );
    }

    /** @var array<string, string> $result */
    $result = [];
    while (count($requirePackages)) {
        $dependency =  array_shift($requirePackages);
        $result[$dependency] = $dependency;

        foreach ($packageDependencies[$dependency] as $dep) {
            if (empty($result[$dep])) {
                $requirePackages[] = $dep;
            }
        }
    }

    return $result;
}
